﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class EnemyZombies : MonoBehaviour
{
    public Animator animator;
    public GameObject player;
    NavMeshAgent nav;
    Vector3 puntoDestino;
    public float health = 50f;
    public ScoreController ScoreController;
    public GameObject municion;
    public float distanciaPerseguir;
    private Transform dropPosition;
    public AudioClip zombieAttack;
    public AudioSource audio;
    public AudioClip zombieDead;
    public AchivementsController AchievementsController;
    private Scene scene;
    private float nextAttack = 0f;
    private float coolDown = 3.0f;

    // Start is called before the first frame update
    void Start(){
        animator = this.gameObject.GetComponent<Animator>();
        player=GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<NavMeshAgent>(); 
        ScoreController = GetComponent<ScoreController>();
        audio = GetComponent<AudioSource>();
        animator.SetBool("Attack", false);
        animator.SetBool("Walk", false);
    }

    // Update is called once per frame
    void Update()
    {
        scene = SceneManager.GetActiveScene();
        nav.destination = player.transform.position;
        float distZombie = Vector3.Distance(player.transform.position, transform.position);
        animator.SetBool("Idle", true);
        animator.SetBool("Walk", false);
        animator.SetBool("Attack", false);
        if(distZombie<distanciaPerseguir){
            nav.isStopped = false;
            animator.SetBool("Walk", true);
            animator.SetBool("Attack", false);
            animator.SetBool("Idle", false);
            if(distZombie<4 && Time.time > nextAttack){
                animator.SetBool("Walk", false);
                animator.SetBool("Attack", true);
                audio.PlayOneShot(zombieAttack,1.0f);
                nextAttack = Time.time + coolDown;
            }else{
                animator.SetBool("Attack", false);
            }
           
        }else{
            nav.isStopped = true;
            animator.SetBool("Attack", false);
            animator.SetBool("Walk", false);
            animator.SetBool("Idle", true);
        }

        if(health<=0f){
            animator.SetBool("Attack", false);
            animator.SetBool("Walk", false);
            animator.SetBool("Idle", false);
            nav.isStopped = true;
        }
    }

    public void takeDamage(float amount)
    {
        health -= amount;
        audio.PlayOneShot(zombieDead,1.0f);
        if (health==0f)
        {    
            StartCoroutine(morir());    
        }
    }

    IEnumerator morir(){
        nav.isStopped = true;
        animator.SetBool("Death",true);
        ScoreController.ActualizarScore();
        AchievementsController.actualizarKills();
        yield return new WaitForSeconds(3f);
        Quaternion rotation = new Quaternion (90,90,0,0);
        Instantiate(municion, transform.position, rotation);
        Destroy(gameObject);
        
    }
}
