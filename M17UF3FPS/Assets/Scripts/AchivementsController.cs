﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AchivementsController : MonoBehaviour
{
    // Start is called before the first frame update
    public int numPartidas;
    public int numKills;
    public int numDeaths;
    public bool tenKills;
    public bool fiftyKills;
    public bool hundredKills;
    public bool knifeKill;
    public bool twentyKills;
    public bool passTutorial;
    public bool passMap_v1;
    public bool passGame;
    public bool buyWeapon;
    public bool topSecretObject;
    private bool mostradoTenKills = false;
    private bool mostradoFiftyKills= false;
    private bool mostradoHundredKills= false;
    private bool mostradoTopSecretKills= false;
    private bool mostradoTwentyKills= false;
    private bool mostradoKnifeKills= false;
    private bool mostradoBuyWeapon = false;
    private bool mostradoPassTutorial= false;
    private bool mostradoPassMap_v1= false;
    private bool mostradoPassGame= false;
    public PickUpWeapon PickUpWeapon;
    public Boosters Boosters;
    public GameObject canvas;
    private Estadisticas estadisticas;
    Scene scene;
    int numPartidasIniciales;
    private bool guardado = false;
    public ScoreController ScoreController;

    void Start()
    {
        ScoreController = GetComponent<ScoreController>();
        canvas.SetActive(false);
        string jsonString = PlayerPrefs.GetString("data6");
        estadisticas = JsonUtility.FromJson<Estadisticas>(jsonString);
        if(estadisticas!= null){
            numPartidas = estadisticas.games;
            numPartidasIniciales = numPartidas;
            numKills = estadisticas.kills;
            numDeaths = estadisticas.deaths;
            tenKills = estadisticas.tenKills;
            fiftyKills = estadisticas.fiftyKills;
            hundredKills = estadisticas.hundredKills;
            knifeKill = estadisticas.knifeKill;
            twentyKills = estadisticas.twentyKills;
            passTutorial = estadisticas.passTutorial;
            passMap_v1 = estadisticas.passMap_v1;
            passGame = estadisticas.passGame;
            buyWeapon = estadisticas.buyWeapon;
            topSecretObject = estadisticas.topSecretObject;
            Debug.Log(JsonUtility.ToJson(estadisticas));
        }

    }

    // Update is called once per frame
    void Update()
    {        
        scene = SceneManager.GetActiveScene();
        if(!buyWeapon){
           buyWeapon = PickUpWeapon.compraArma(); 
        }else{
            mostradoBuyWeapon = true;
        }
        if(!topSecretObject){
            topSecretObject = Boosters.isRecogerFolder();
        }else{
            mostradoTopSecretKills = true;
        }
        if(estadisticas != null){
             if(estadisticas.tenKills){
                mostradoTenKills = true;
            }

            if(estadisticas.twentyKills){
                mostradoTwentyKills = true;
            }

            if(estadisticas.fiftyKills){
                mostradoFiftyKills = true;
            }

            if(estadisticas.hundredKills){
                mostradoHundredKills = true;

            }

            if(estadisticas.passGame){
                mostradoPassGame = true;
            }
            if(estadisticas.passTutorial){
                mostradoPassTutorial = true;
            }
            if(estadisticas.passMap_v1){
                mostradoPassMap_v1 = true;
            }
        }

         if(scene.name == "Map_v1" || scene.name == "Hangar" || scene.name == "ThirdLevel" || scene.name == "GameWin"){
            List<Text> textos = new List<Text>();
            canvas.GetComponentsInChildren<Text>(textos);
            foreach (Text text in textos)
            {
                if(text.name == "AchivementText"){
                    if(buyWeapon && !mostradoBuyWeapon){
                        canvas.SetActive(true);
                        text.text = "Capitalsimo";
                        StartCoroutine(desactivarPanel());
                        mostradoBuyWeapon=true;
                    }

                    if(numKills == 20 && !mostradoTwentyKills){
                        canvas.SetActive(true);
                        text.text = "Asesino II";
                        StartCoroutine(desactivarPanel());
                        mostradoTwentyKills=true;
                    }

                    if(knifeKill&& !mostradoKnifeKills){
                        canvas.SetActive(true);
                        text.text = "Acuchillador";
                        StartCoroutine(desactivarPanel());
                        mostradoKnifeKills=true;
                    }

                    if(passTutorial&& !mostradoPassTutorial){
                        canvas.SetActive(true);
                        text.text = "Primeros Pasos";
                        StartCoroutine(desactivarPanel());
                        mostradoPassTutorial=true;
                    }

                    if(topSecretObject&& !mostradoTopSecretKills){
                        Debug.Log(topSecretObject);
                        canvas.SetActive(true);
                        text.text = "Top Secret";
                        StartCoroutine(desactivarPanel());
                        mostradoTopSecretKills=true;
                    }
                    if(passMap_v1&& !mostradoPassMap_v1){
                        canvas.SetActive(true);
                        text.text = "Hora de acabar";
                        StartCoroutine(desactivarPanel());
                        mostradoPassMap_v1=true;
                    }
                    if(passGame && !mostradoPassGame){
                        canvas.SetActive(true);
                        text.text = "Completado!";
                        StartCoroutine(desactivarPanel());
                        mostradoPassGame=true;
                    }
                    if(numKills == 10 && !mostradoTenKills){
                        canvas.SetActive(true);
                        tenKills = true;
                        text.text = "Asesio I";
                        StartCoroutine(desactivarPanel());
                        mostradoTenKills=true;
                    }
                    if(numKills == 50&& !mostradoFiftyKills){
                        canvas.SetActive(true);
                        fiftyKills = true;
                        text.text = "Asesino III";
                        StartCoroutine(desactivarPanel());
                        mostradoFiftyKills=true;
                    }
                    if(numKills == 100&& !mostradoHundredKills){
                        canvas.SetActive(true);
                        hundredKills = true;
                        text.text = "Experto Asesino";
                        StartCoroutine(desactivarPanel());
                        mostradoHundredKills=true;
                    }
                }
            }
        }
       
        
        if(scene.name == "Map_v1"){
            passTutorial = true;
            
        }else if(scene.name == "ThirdLevel"){
            passMap_v1 = true;
        }else if(scene.name == "GameWin"){
            passGame = true;
            if(!guardado){
                guardadoEntreEscenas();
                guardado = true;
            }
            
        }

       

        if(ScoreController.numZombies == 0){
            guardadoEntreEscenas();
        }
        
    }

    public void actualizarKills(){
        numKills+=1;
    }

    public void actualizarDeaths(){
        numDeaths+=1;
    }

    public void incrementarPartidas(){
        numPartidas= numPartidasIniciales+1;
    }

    public void guardar(){
        Estadisticas estadisticas = new Estadisticas();
        estadisticas.games += numPartidas;
        estadisticas.kills += numKills;
        estadisticas.deaths += numDeaths;
        estadisticas.tenKills = tenKills;
        estadisticas.fiftyKills = fiftyKills;
        estadisticas.hundredKills = hundredKills;
        estadisticas.knifeKill = knifeKill;
        estadisticas.twentyKills = twentyKills;
        estadisticas.passTutorial = passTutorial;
        estadisticas.passMap_v1 = passMap_v1;
        estadisticas.passGame = passGame;
        estadisticas.buyWeapon = buyWeapon;
        estadisticas.topSecretObject = topSecretObject;
        PlayerPrefs.SetString("data5", JsonUtility.ToJson(estadisticas));
        PlayerPrefs.Save();
        Debug.Log(JsonUtility.ToJson(estadisticas));
    }

    public void guardadoEntreEscenas(){
        if(scene.name == "Map_v1"){
            incrementarPartidas();
        }
        Estadisticas estadisticas = new Estadisticas();
        estadisticas.games += numPartidas;
        estadisticas.kills += numKills;
        estadisticas.deaths += numDeaths;
        estadisticas.tenKills = tenKills;
        estadisticas.fiftyKills = fiftyKills;
        estadisticas.hundredKills = hundredKills;
        estadisticas.knifeKill = knifeKill;
        estadisticas.twentyKills = twentyKills;
        estadisticas.passTutorial = passTutorial;
        estadisticas.passMap_v1 = passMap_v1;
        estadisticas.passGame = passGame;
        estadisticas.buyWeapon = buyWeapon;
        estadisticas.topSecretObject = topSecretObject;
        PlayerPrefs.SetString("data6", JsonUtility.ToJson(estadisticas));
        PlayerPrefs.Save();
        Debug.Log(scene.name + JsonUtility.ToJson(estadisticas));

    }

    IEnumerator desactivarPanel(){
        yield return new WaitForSeconds(4f);
        canvas.SetActive(false);
    }


}   
[System.Serializable]
public class Estadisticas{
    public int games;
    public int kills;
    public int deaths;
    public bool tenKills;
    public bool fiftyKills;
    public bool hundredKills;
    public bool knifeKill;
    public bool twentyKills;
    public bool passTutorial;
    public bool passMap_v1;
    public bool passGame;
    public bool buyWeapon;
    public bool topSecretObject;
}
