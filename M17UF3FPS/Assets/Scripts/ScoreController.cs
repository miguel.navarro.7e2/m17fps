﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreController : MonoBehaviour
{
    public static int coins = 0;
    public static int numZombies=43;
    Scene scene;
    public GameObject playerCanvas;
    // Start is called before the first frame update
    void Start()
    { 
        scene = SceneManager.GetActiveScene();
        if(scene.name == "Hangar"){
            numZombies = 4;

        }else if(scene.name == "Map-v1"){
            numZombies = 43;
        }
        List<Text> textos = new List<Text>();
        playerCanvas.GetComponentsInChildren<Text>(textos);

        foreach (Text text in textos)
        {
            if(text.name == "NumZombiesText"){
                text.text = numZombies + "";
            }else if(text.name == "NumCoinsText"){
                text.text = coins + "";
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ActualizarScore(){
        numZombies-=1;
        coins+=50;
        List<Text> textos = new List<Text>();
        playerCanvas.GetComponentsInChildren<Text>(textos);

        foreach (Text text in textos)
        {
            if(text.name == "NumZombiesText"){
                text.text = numZombies + "";
            }else if(text.name == "NumCoinsText"){
                text.text = coins + "";
            }
        }
    }

    public static void setNumZombies(int numZombiesParam){
        numZombies = numZombiesParam;
    }

    public void RestarScore(int precioCompra){
        coins -= precioCompra;
        List<Text> textos = new List<Text>();
        playerCanvas.GetComponentsInChildren<Text>(textos);

        foreach (Text text in textos)
        {
            if(text.name == "NumZombiesText"){
                text.text = numZombies + "";
            }else if(text.name == "NumCoinsText"){
                text.text = coins + "";
            }
        }
    }
}
