﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpAmmo : MonoBehaviour
{

    public Collider B1;
    public Arma arma;
    public int numMunicion = 5;
    public int ammo;
    public WeaponSwitch WeaponHolder;
    private AudioSource audio;
    public AudioClip pickUpSound;
    public Text ammoFullText;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }
 
    // Update is called once per frame
    void Update()
    {
        arma = WeaponHolder.GetWeaponSelected().GetComponent<Arma>();
        if(arma != null){
            ammo = arma.fullAmmo;
        }
    }
 
    void OnTriggerEnter(Collider other){
        if(other.tag == "Player"){
            arma.fullAmmo += numMunicion;
            ammoFullText.text = arma.fullAmmo.ToString();
            audio.PlayOneShot(pickUpSound,1.0f);
            StartCoroutine(destruirObjeto());
            
        }
    }

    IEnumerator destruirObjeto(){
        yield return new WaitForSeconds(1.1f);
        Destroy(gameObject); 
    }
}
