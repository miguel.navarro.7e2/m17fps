﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameStates{
    menu,
    playing,
    pause,
    gameOver,
    gameWin
}

public class GameManagerController : MonoBehaviour
{
    public static GameManagerController instance;
    
    public GameStates gameState;

    Scene scene;

    public ScoreController ScoreController;

    public int playerHitResistance = 3 ;

    public int lives = 0;

    public int bulletproof = 0;

    public GameObject equipmentPanel;

    AudioSource audio;

    public AudioClip[] listAudio;

    private void Awake(){
        if(instance==null){
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }else{
            Destroy(this);
        }
        gameState = GameStates.playing;
    } 

    void Start()
    {
        scene = SceneManager.GetActiveScene();
        ScoreController = GetComponent<ScoreController>();
        if(scene.name == "Map_v1"){
            ScoreController.setNumZombies(43);
        }
        List<Text> textos = new List<Text>();
        equipmentPanel.GetComponentsInChildren<Text>(textos);

        foreach (Text text in textos)
        {
            if(text.name == "NumMedkitText"){
                text.text = lives + "";
            }else if(text.name == "NumBulletProofText"){
                text.text = bulletproof + "";
            }
        }
        audio = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        scene = SceneManager.GetActiveScene();
        if(scene.name == "MenuPrincipal"){
            playerHitResistance = 3;
            bulletproof = 0;
            lives = 0;
            ScoreController.setNumZombies(4);
        }
        if(scene.name == "Hangar"){
            audio.clip = listAudio[0];
            if(!audio.isPlaying){
                audio.Play();
            }
        }else if(scene.name == "Map_v1"){
            audio.clip = listAudio[1];
            if(!audio.isPlaying){
                audio.Play();
            }
        }else if(scene.name == "ThirdLevel"){
            audio.clip = listAudio[2];
            if(!audio.isPlaying){
                audio.Play();
            }
        }else{
            audio.Stop();
        }
        if(ScoreController.numZombies == 0 && scene.name == "Hangar"){
            StartCoroutine(cambioEscenaMap_V1());
        }else if(ScoreController.numZombies == 0 && scene.name == "Map_v1"){
            StartCoroutine((cambioEscenaThirdLevel()));
        }else if (ScoreController.numZombies == 0 && scene.name == "ThirdLevel"){
            StartCoroutine((cambioEscenaGameWin()));
        }else if (scene.name == "GameOver" || scene.name == "GameWin" ){
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        
    }

    public GameStates getGameState(){
        return this.gameState;
    }

    public void setGameStates(GameStates _gm){
        this.gameState = _gm;
        gameStateUpdate();
    }

    void gameStateUpdate(){
        switch(gameState){
            case GameStates.pause:
                setGameInPause();
                break;
            case GameStates.gameOver:
                setGameInGameOver();
                break;
            case GameStates.gameWin:
                setGameInGameWin();
                break;
            case GameStates.menu:
                setGameInMenu();
                break;
            default:
                setGameInPlaying();
                break;
        }
    }

    
    private void setGameInMenu(){
        playerHitResistance = 3;
    }


    private void setGameInPlaying()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    private void setGameInGameWin()
    {
        SceneManager.LoadScene("GameWin");
        playerHitResistance = 3;
    }

    private void setGameInGameOver()
    {  
        if(bulletproof>0){
            bulletproof-=1;
            List<Text> textos = new List<Text>();
            equipmentPanel.GetComponentsInChildren<Text>(textos);

            foreach (Text text in textos)
            {
                if(text.name == "NumMedkitText"){
                    text.text = lives + "";
                }else if(text.name == "NumBulletProofText"){
                    text.text = bulletproof + "";
                }
            } 
        }else{
            this.playerHitResistance -=1;
            if(this.playerHitResistance<=0 && lives==0){
                StartCoroutine(cambiarGameObver());
            }
            if(playerHitResistance<=0 && lives >0){
                lives -=1;
                List<Text> textos = new List<Text>();
                equipmentPanel.GetComponentsInChildren<Text>(textos);

                foreach (Text text in textos)
                {
                    if(text.name == "NumMedkitText"){
                        text.text = lives + "";
                    }else if(text.name == "NumBulletProofText"){
                        text.text = bulletproof + "";
                    }
                }
                playerHitResistance = 3;
            }
        }
    }

    public int getPlayerHitResistance(){
        return playerHitResistance;
    }

    private void setGameInPause()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        //throw new NotImplementedException();
    }

    IEnumerator cambioEscenaMap_V1(){
        yield return new WaitForSeconds(2.5f);
        if(scene.name=="Hangar"){
            ScoreController.setNumZombies(43);
            SceneManager.LoadScene("Map_v1");
        }
    }

    IEnumerator cambioEscenaThirdLevel(){
        yield return new WaitForSeconds(2.5f);
        if(scene.name == "Map_v1"){
            ScoreController.setNumZombies(27);
            SceneManager.LoadScene("ThirdLevel");
        }
        
    }

    IEnumerator cambioEscenaGameWin(){
        yield return new WaitForSeconds(2.5f);
        if(scene.name == "ThirdLevel"){
            setGameInGameWin();
        }
    }

    IEnumerator cambiarGameObver(){
        yield return new WaitForSeconds(1.3f);
        SceneManager.LoadScene("GameOver");
        playerHitResistance = 3;
        ScoreController.setNumZombies(43);
    }

    public void playAgain(){
        ScoreController.setNumZombies(43);
    }
}
