﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpWeapon : MonoBehaviour
{
   public ScoreController ScoreController;
    public WeaponSwitch WeaponSwitch;
    public int precio;
    public GameObject mensajeCompra;
    public Collider B1;
    public Collider B2;
    public GameObject armaAEliminarInHolder;
    public GameObject armaAComprar;
    public Transform parent;
    public AudioSource audio;
    public AudioClip[] listAudio; 
    private bool comprardo = false;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        B1.enabled = true;
        B2.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay(Collider other){
        if(other.tag == "Player"){
            mensajeCompra.GetComponent<Text>().text =  "(" + precio + " coins): " + " K para comprar";
        }

        if(other.tag == "Player" && ScoreController.coins >=precio && Input.GetKeyDown(KeyCode.K) && !B1.enabled){
            ScoreController.RestarScore(precio);
            B1.enabled = false;
            B2.enabled = true;
            armaAEliminarInHolder = WeaponSwitch.GetWeaponSelected();
            mensajeCompra.GetComponent<Text>().text = "";
            Destroy(armaAEliminarInHolder);
            armaAComprar.transform.SetParent(parent);
            armaAComprar.SetActive(true);
            WeaponSwitch.SetWeapon(armaAComprar);
            mensajeCompra.GetComponent<Text>().text = "";
            comprardo = true;
            mensajeCompra.GetComponent<Text>().text = "";
            StartCoroutine(AudioSound());
        }

        if(ScoreController.coins < precio && Input.GetKeyDown(KeyCode.K) && !B1.enabled){
            audio.clip = listAudio[1];
            audio.Play();
        }
    }

    void OnTriggerExit(Collider other){
        if(other.tag == "Player"){
            mensajeCompra.GetComponent<Text>().text = "";
        }
    }

    public bool compraArma(){
        if(comprardo){
            return true;
        }else{
            return false;
        }
        
    }

    IEnumerator AudioSound(){
        mensajeCompra.GetComponent<Text>().text = "";
        audio.clip = listAudio[0];
        audio.Play();
        yield return new WaitForSeconds(0.8f);
        this.gameObject.SetActive(false);
    }
}
