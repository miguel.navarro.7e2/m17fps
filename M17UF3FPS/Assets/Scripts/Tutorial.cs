﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{

    public GameObject panelTutorial;
    public Collider part2;
    public Collider part3;
    public Collider part4;
    public PickUpWeapon pickUpWeapon;
    public PickUpWeapon pickUpWeapon2;
    public Boosters boosters;
    public Boosters boosters2;
    public ScoreController ScoreController;
    private bool comprarArma = false;
    private bool recogerEscudo = false;
    private bool recogerMedkit = false;
    private bool comprarArma2 = false;

    Text tutorialText;
    // Start is called before the first frame update
    void Start()
    {
        tutorialText = panelTutorial.GetComponent<Text>();
        tutorialText.text = "Compra el arma de la pared";
        ScoreController.coins = 100;
    }

    // Update is called once per frame
    void Update()
    {
        comprarArma = pickUpWeapon.compraArma();
        recogerMedkit = boosters.recogido();
        recogerEscudo = boosters2.recogido();
        comprarArma2 = pickUpWeapon2.compraArma();
        if(comprarArma){
            //Sonido al poner collider a trigger
            part2.isTrigger = true;
            tutorialText.text = "Recoge un Medkit y un Escudo";
        }

        if(recogerEscudo && recogerMedkit){
            //Sonido
            part3.isTrigger = true;
            tutorialText.text = "Cambia de arma con la rueda del ratón y compra el siguiente arma";
        }

        if(comprarArma2){
            //Sonido
            part4.isTrigger = true;
            tutorialText.text = "Elimina todos los Zombies para finalizar el tutorial";
        }

    }
}
