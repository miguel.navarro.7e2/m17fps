﻿using UnityEngine;
using UnityEngine.UI;

public class WeaponSwitch : MonoBehaviour
{

    public int selectedWeapon = 0;
    public Transform weapon;
    public Animator Animator;
    public Arma Arma1;
    public Arma Arma2;

    public Image image;
    public Sprite ak47;
    public Sprite m4a1;
    public Sprite pistol;
    public Sprite skorpion;
    public Sprite ump;
    public Sprite bullpup;
    public Sprite manos;

    void Start()
    {
        Animator = GetComponent<Animator>();
        SelectWeapon();
        Arma1 = GetComponent<Arma>();
        Arma2 = GetComponent<Arma>();
    }

    // Update is called once per frame
    void Update()
    {

        int previousSelected = selectedWeapon;

        if(Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if(selectedWeapon >= transform.childCount - 1)
            {
                selectedWeapon=0;
            }
            else
            {
                selectedWeapon++;
            } 
        }
        if(Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if(selectedWeapon <= 0)
            {
                selectedWeapon = transform.childCount - 1;
            }
            else
            {
                selectedWeapon--;
            } 
        }
        if(previousSelected != selectedWeapon)
        {
            SelectWeapon();
        }

        name = weapon.gameObject.GetComponent<Arma>().weaponName;
        if(name=="ak47"){
            image.sprite = ak47;
        }
        if(name=="m4a1"){
            image.sprite = m4a1;
        }
        if(name=="skorpion"){
            image.sprite = skorpion;
        }
        if(name=="bullpup"){
            image.sprite = bullpup;
        }
        if(name=="ump"){
            image.sprite = ump;
        }
        if(name == "gun"){
            image.sprite = pistol;
        }
        
    }

    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weaponTr  in transform)
        {
            if(i==selectedWeapon)
            {
                weaponTr.gameObject.SetActive(true);
                weapon = weaponTr;
            }
            else
            {
                weaponTr.gameObject.SetActive(false);
            }
            i++;
        }
        
        Animator.SetBool("Reloading",false);
        weapon.gameObject.GetComponent<Arma>().isReloading = false;
        name = weapon.gameObject.GetComponent<Arma>().weaponName;
        if(name=="ak47"){
            image.sprite = ak47;
        }
        if(name=="m4a1"){
            image.sprite = m4a1;
        }
        if(name=="skorpion"){
            image.sprite = skorpion;
        }
        if(name=="bullpup"){
            image.sprite = bullpup;
        }
        if(name=="ump"){
            image.sprite = ump;
        }
        if(name == "gun"){
            image.sprite = pistol;
        }
    }

    public  GameObject GetWeaponSelected(){
        return this.weapon.gameObject;
    }

    public void SetWeapon(GameObject _gameObject){
        this.weapon = _gameObject.transform;
    }
   
}
