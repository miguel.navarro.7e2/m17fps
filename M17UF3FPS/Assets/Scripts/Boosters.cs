﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Boosters : MonoBehaviour
{
    public GameObject booster;
    public GameObject infoText;
    private bool isRecogido;
    private bool recogerFolder = false;
    private AudioSource audio;
    public AudioClip pickUpSound;
    
    public GameObject playerCanvas;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        List<Text> textos = new List<Text>();
        playerCanvas.GetComponentsInChildren<Text>(textos);

        foreach (Text text in textos)
        {
            if(text.name == "NumBulletproofText"){
                text.text = GameManagerController.instance.bulletproof + "";
            }else if(text.name == "NumMedkitText"){
                text.text = GameManagerController.instance.lives + "";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay(Collider other){
        if(other.tag == "Player" && booster.name == "MedKit"){
            infoText.GetComponent<Text>().text = "Medkit: K para coger";
        }

        if(other.tag == "Player" && Input.GetKeyDown(KeyCode.K) & booster.name == "MedKit"){
            audio.PlayOneShot(pickUpSound,1.0f);
            GameManagerController.instance.lives +=1;
            isRecogido = true;
            infoText.GetComponent<Text>().text = "";
            List<Text> textos = new List<Text>();
            playerCanvas.GetComponentsInChildren<Text>(textos);

            foreach (Text text in textos)
            {
                if(text.name == "NumBulletproofText"){
                    text.text = GameManagerController.instance.bulletproof + "";
                }else if(text.name == "NumMedkitText"){
                    text.text = GameManagerController.instance.lives + "";
                }
            }
            StartCoroutine(destruirObjecto());
        }

        if(other.tag == "Player" && booster.name == "Bulletproof"){
            infoText.GetComponent<Text>().text = "Bulletproof: K para coger";
        }

        if(other.tag == "Player" && Input.GetKeyDown(KeyCode.K) & booster.name == "Bulletproof"){
            audio.PlayOneShot(pickUpSound,1.0f);
            infoText.GetComponent<Text>().text = "";
            GameManagerController.instance.bulletproof +=1;
            isRecogido = true;
            List<Text> textos = new List<Text>();
        playerCanvas.GetComponentsInChildren<Text>(textos);

        foreach (Text text in textos)
        {
            if(text.name == "NumBulletproofText"){
                text.text = GameManagerController.instance.bulletproof + "";
            }else if(text.name == "NumMedkitText"){
                text.text = GameManagerController.instance.lives + "";
            }
        }
        StartCoroutine(destruirObjecto());
        }

        if(other.tag == "Player" && booster.name == "folder"){
            infoText.GetComponent<Text>().text = "Secret Folder: K para coger";
        }

        if(other.tag == "Player" && Input.GetKeyDown(KeyCode.K) & booster.name == "folder"){
            audio.PlayOneShot(pickUpSound,1.0f);
            infoText.GetComponent<Text>().text = "";
            recogerFolder = true;
            StartCoroutine(destruirObjecto());
        }
    }

    void OnTriggerExit(Collider other){
        if(other.tag == "Player"){
            infoText.GetComponent<Text>().text = "";
        }
    }

    public bool recogido(){
        if(isRecogido){
            return true;
        }else{
            return false;
        }
    }

    public bool isRecogerFolder(){
        if(recogerFolder){
            return true;
        }else{
            return false;
        }
    }

    IEnumerator destruirObjecto(){
        infoText.GetComponent<Text>().text = "";
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
        infoText.GetComponent<Text>().text = "";
    }
}
