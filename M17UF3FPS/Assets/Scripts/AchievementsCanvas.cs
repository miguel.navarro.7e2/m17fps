﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsCanvas : MonoBehaviour
{
    public AchivementsController AchievementsController;
    public Text numGamesText;
    public Text numKillsText;
    public Text numDeathsText;
    public Image comprarArma;
    public Image twentyKills;
    public Image putSecretObject;
    public Image fiftyKills;
    public Image knifeKill;
    public Image hundredKills;
    public Image map_v1Complete;
    public Image passTutorial;
    public Image tenKills;
    public Image passGame;
    public Image comprarArmaLock;
    public Image twentyKillsLock;
    public Image putSecretObjectLock;
    public Image fiftyKillsLock;
    public Image knifeKillLock;
    public Image hundredKillsLock;
    public Image map_v1CompleteLock;
    public Image passTutorialLock;
    public Image tenKillsLock;
    public Image passGameLock;
    Estadisticas estadisticas;
    // Start is called before the first frame update
    void Start()
    {
        string jsonString = PlayerPrefs.GetString("data6");
        estadisticas = JsonUtility.FromJson<Estadisticas>(jsonString);
        Debug.Log(JsonUtility.ToJson(estadisticas));
        if(estadisticas== null){
            numDeathsText.text = 0 + "";
            numGamesText.text = 0 + "";
            numKillsText.text = 0 + "";
        }else{
            numKillsText.text = estadisticas.kills + "";
        numGamesText.text = estadisticas.games + "";
        numDeathsText.text = estadisticas.deaths + "";
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if(estadisticas!= null){
                    if(estadisticas.buyWeapon == true){
                comprarArma.gameObject.SetActive(true);
                comprarArmaLock.gameObject.SetActive(false);
            }else{
                comprarArmaLock.gameObject.SetActive(true);
            }

            if(estadisticas.kills>=20){
                twentyKills.gameObject.SetActive(true);
                twentyKillsLock.gameObject.SetActive(false);
            }else{
                twentyKillsLock.gameObject.SetActive(true);
            }

            if(estadisticas.topSecretObject){
                putSecretObject.gameObject.SetActive(true);
                putSecretObjectLock.gameObject.SetActive(false);
            }else{
                putSecretObjectLock.gameObject.SetActive(true);
            }

            if(estadisticas.passGame){
                passGame.gameObject.SetActive(true);
                passGameLock.gameObject.SetActive(false);
            }else{
                passGameLock.gameObject.SetActive(true);
            }

            if(estadisticas.passTutorial){
                passTutorial.gameObject.SetActive(true);
                passTutorialLock.gameObject.SetActive(false);
            }else{
                passTutorialLock.gameObject.SetActive(true);
            }
            
            if(estadisticas.passMap_v1){
                map_v1Complete.gameObject.SetActive(true);
                map_v1CompleteLock.gameObject.SetActive(false);
            }else{
                map_v1CompleteLock.gameObject.SetActive(true);
            }
            
            if(estadisticas.kills>=10){
                tenKills.gameObject.SetActive(true);
                tenKillsLock.gameObject.SetActive(false);
            }else{
                tenKillsLock.gameObject.SetActive(true);
            }
            
            if(estadisticas.kills >= 50){
                fiftyKills.gameObject.SetActive(true);
                fiftyKillsLock.gameObject.SetActive(false);
            }else{
                fiftyKillsLock.gameObject.SetActive(true);
            }

            if(estadisticas.kills >= 100){
                hundredKills.gameObject.SetActive(true);
                hundredKillsLock.gameObject.SetActive(false);
            }else{
                hundredKillsLock.gameObject.SetActive(true);
            }

            if(estadisticas.knifeKill){
                knifeKill.gameObject.SetActive(true);
                knifeKillLock.gameObject.SetActive(false);
            }else{
                knifeKillLock.gameObject.SetActive(true);
            }
        }
       
    }
}
