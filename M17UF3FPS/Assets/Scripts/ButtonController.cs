﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{

    Scene scene;
    public PauseMenu1 PauseMenu1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update(){
        scene = SceneManager.GetActiveScene(); 
    }

    public void playButton(){
        SceneManager.LoadScene("Hangar");
    }

    public void quitButton(){
        if(scene.name=="MenuPrincipal"){
            Application.Quit();
        }else{
            SceneManager.LoadScene("MenuPrincipal");
            GameManagerController.instance.setGameStates(GameStates.menu);
        }   
    }

    public void resumeButton(){
        PauseMenu1.DescativateMenu();
    }

    public void logrosButton(){
        SceneManager.LoadScene("Achievements");
    }

    public void controlsButton(){
        SceneManager.LoadScene("Controls");
    }

    public void playAgainButton(){
        GameManagerController.instance.playerHitResistance = 3;
        SceneManager.LoadScene("Map_v1");
        GameManagerController.instance.setGameStates(GameStates.playing);
        GameManagerController.instance.playAgain();
    
    }
}
