﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCOntroller : MonoBehaviour
{

    AudioSource audio;
    public AudioClip[] listAudio;
    Scene scene;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        scene = SceneManager.GetActiveScene();
        if(scene.name == "MenuPrincipal"){
            audio.clip = listAudio[0];
            if(!audio.isPlaying){
                audio.Play();
            }
        }
    }
}
