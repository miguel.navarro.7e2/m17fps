﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class OptionController : MonoBehaviour
{
    public string fileName = "volumeValue";
    public AudioMixer mixer;

    void Start(){
        float value = PlayerPrefs.GetFloat(fileName);
        SetLevel(value);
        Debug.Log(value);
    }
    
    public void SetLevel(float sliderValue){
        mixer.SetFloat("VolValue", Mathf.Log10(sliderValue)*20);
        PlayerPrefs.SetFloat(fileName, sliderValue );
        PlayerPrefs.Save();
    }
}
