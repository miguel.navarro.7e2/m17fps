﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu1 : MonoBehaviour
{
    [SerializeField] public GameObject pauseMenuUI;

    public GameObject gamePanel;

    [SerializeField] private bool isPaused;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            isPaused = !isPaused;
        }
        if(isPaused){
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            GameManagerController.instance.setGameStates(GameStates.pause);
            gamePanel.SetActive(false);
            ActivateMenu();
        }else{
            DescativateMenu();
            gamePanel.SetActive(true);
        }
    }

    void ActivateMenu(){
        Time.timeScale = 0;
        AudioListener.pause = true;
        pauseMenuUI.SetActive(true);
    }

    public void DescativateMenu(){
        Time.timeScale = 1;
        AudioListener.pause = false;
        isPaused = false;
        pauseMenuUI.SetActive(false);
        GameManagerController.instance.setGameStates(GameStates.playing);
    }
}
