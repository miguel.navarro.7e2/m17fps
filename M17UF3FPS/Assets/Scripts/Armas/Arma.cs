﻿ using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Arma : MonoBehaviour
{
    public String weaponName;
    public float damage = 10f;
    public float range = 100f;
    public float fireRate;
    public Camera fpsCamera;
    public Animator animator;
    public Animator animatorHolder;
    public AudioSource _audio;
    public int maxAmmo = 30;
    public int currentAmmo;
    public float reloadTime = 1.5f;
    public AudioClip reloadAudio;
    public bool isReloading = false;
    private float nextTimeToFire = 0f;
    public int fullAmmo = 120;
    public int bulletsTo = 0;

    public ParticleSystem _particleSystem;

    public Text ammoText;
    public Text ammoFullText;

   
    void Start()
    {
        animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
        _particleSystem = GetComponent<ParticleSystem>();
        currentAmmo = maxAmmo;
        ammoText.text = currentAmmo.ToString();
        ammoFullText.text = fullAmmo.ToString();
    }

    void FixedUpdate()
    {
        ammoText.text = currentAmmo.ToString();
        if(isReloading)
        {
            return;
        }
        if(currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }
        if(Input.GetKeyDown(KeyCode.R)){
            StartCoroutine(Reload());
            return;
        }
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && fullAmmo>0)
        {
            _audio.PlayOneShot(_audio.clip, 1.0f);
            _particleSystem.Emit(1);
            nextTimeToFire = Time.time + 1f /fireRate;
            animator.SetBool("Shoot", true);
            Shoot();
        }else{
            animator.SetBool("Shoot", false);
            animatorHolder.SetBool("Retroceso", false);
            animatorHolder.SetBool("Retroceso2", false);
            animatorHolder.SetBool("Retroceso3", false);
        }
    }

    IEnumerator Reload()
    {
        animatorHolder.SetBool("Retroceso", false);
        animatorHolder.SetBool("Retroceso2", false);
        animatorHolder.SetBool("Retroceso3", false);
        animator.SetBool("Shoot", false);
        isReloading = true;
        animatorHolder.SetBool("Reloading",true);
        animator.SetBool("Reloading",true);
        _audio.PlayOneShot(reloadAudio, 1.0f);
        
        yield return new WaitForSeconds(reloadTime);

        animatorHolder.SetBool("Reloading",false);
        animator.SetBool("Reloading",false);
        bulletsTo = maxAmmo - currentAmmo;
        currentAmmo = maxAmmo;
        fullAmmo -= bulletsTo; 
        isReloading = false;
        ammoFullText.text = fullAmmo.ToString();
        ammoText.text = currentAmmo.ToString();
    }

    private void Shoot()
    {
        if(gameObject.name == "Bullpup"|| gameObject.name == "UMP"){
            animatorHolder.SetBool("Retroceso2", true);
        }else if(gameObject.name == "M4a1" || gameObject.name == "AK-HANDS"){
            animatorHolder.SetBool("Retroceso", true);
        }else{
            animatorHolder.SetBool("Retroceso3", true);
        }
        currentAmmo--;
        ammoText.text = currentAmmo.ToString();
        ammoFullText.text = fullAmmo.ToString();
        RaycastHit hit;
        if(Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            EnemyZombies enemy = hit.transform.GetComponent<EnemyZombies>();
            if(enemy != null)
            {
                enemy.takeDamage(damage);
            }
        }
    }
}
