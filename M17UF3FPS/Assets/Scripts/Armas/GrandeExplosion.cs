﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrandeExplosion : MonoBehaviour
{
    public float radius = 7;
    public float explosionForce = 50;
    public float fuerzaLanzamientoZ = 20f;
    public float fuerzaLanzamientoY = 8;
    private float explosionTime = 2.5f;
    private Collider[] hitColliders;
    public GameObject explosion;
    private AudioSource audio;
    public AudioClip[] audioGranada;

    private Rigidbody rb;

    void Awake(){
        rb = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
    }

    void Update(){
        explosionTime -= Time.deltaTime;
        if(explosionTime <= 0.0f){
            Explode();
            
        }
    }

    void Explode(){
        Instantiate(explosion, transform.position, Quaternion.identity);
        Vector3 center = transform.position;
        hitColliders = Physics.OverlapSphere(center, radius);
        
        foreach (Collider hitCol in hitColliders)
        {
            if(hitCol.GetComponent<Rigidbody>()!=null){
                hitCol.GetComponent<Rigidbody>().isKinematic = false;
                hitCol.GetComponent<Rigidbody>().AddExplosionForce(explosionForce,center,radius,3,ForceMode.Impulse);
                
                if(hitCol.gameObject.name=="Zombie"){
                    EnemyZombies EnemyZombies = hitCol.gameObject.GetComponent<EnemyZombies>();
                    EnemyZombies.takeDamage(50f);
                    Destroy(hitCol.gameObject); 
                }
            }
           
        }
        Destroy(gameObject);

    }

    public void Throw(Vector3 dir){
        rb.AddForce(dir*fuerzaLanzamientoZ, ForceMode.Impulse);
        rb.AddForce(Vector3.up*fuerzaLanzamientoY, ForceMode.Impulse);
    }
    
}
