﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour
{
    public float damage = 50f;
    public float range = 7f;
    public Camera fpsCamera;
    public Animator animatorHolder;
    private Animator animator;
    public AudioSource _audio;
    private float nextTimeToSlash = 0f;
    private float coolDown = 5.0f;
    public float tiempoCooldownAtaque = 5f;
    private string weaponName;


    void Start()
    {
        _audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();

    }

    void FixedUpdate()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToSlash)
        {
            ataque();
            _audio.Play();
        }
    }

    private void SlashAttack()
    {
        RaycastHit hit;
        if(Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            EnemyZombies enemy = hit.transform.GetComponent<EnemyZombies>();
            if(enemy != null)
            {
                enemy.takeDamage(damage);
            }
        }
    }

    private IEnumerator cooldownAtaque()
    {
        yield return new WaitForSeconds(tiempoCooldownAtaque);
        animator.SetBool("Slash", false);
    }

    private void ataque()
    {
        animator.SetBool("Slash", true);
        nextTimeToSlash = Time.time + coolDown;
        SlashAttack();
        StartCoroutine(cooldownAtaque());
    }


}
