﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Granada : MonoBehaviour
{

    [SerializeField]public GrandeExplosion granadaPrefab;

    [SerializeField]public GameObject granadaVisual;

    public Transform myTransform;

    public Camera Camera;

    public int numGranada = 3;

    public bool haveAGranada = true;
    public Text numGrenadesText;
    public AudioClip[] audioGranada;
    public AudioSource audio;
    
    void Start(){
        numGranada = 3;
        haveAGranada = true;
        numGrenadesText.text=numGranada.ToString();
        audio = GetComponent<AudioSource>();
    }

    void Update(){
        if(Input.GetKeyDown(KeyCode.Q) && haveAGranada && numGranada>0){
            ThrowGranada();
            numGranada--;
            numGrenadesText.text=numGranada.ToString();
            StartCoroutine(Audio());
            
        }

        if(numGranada<= 0){
            haveAGranada = false;
        }
    }

    public void ThrowGranada(){
        granadaVisual.SetActive(false);
        GrandeExplosion clone = (GrandeExplosion) Instantiate(granadaPrefab, new Vector3(myTransform.position.x, myTransform.position.y + 0.5f, myTransform.position.z), myTransform.transform.rotation);
        clone.Throw(Camera.transform.forward);
    }

    IEnumerator Audio(){
        audio.clip = audioGranada[0];
        yield return new WaitForSeconds(2.5f);
        audio.Play();
    }

}
